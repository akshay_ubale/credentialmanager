import 'package:credmanage/cred_create_screen.dart';
import 'package:credmanage/models/mode.dart';
import 'package:credmanage/services/encryption.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'models/cred.dart';
import 'services/helper.dart';

class CredWidget extends StatefulWidget {
  final Cred credential;

  CredWidget(this.credential);

  @override
  CredWidgetState createState() => CredWidgetState();
}

class CredWidgetState extends State<CredWidget> {
  bool isExpanded = false;
  List<bool> passwordVisible = [];

  @override
  void initState() {
    isExpanded = false;
    widget.credential.passwords.forEach((element) {
      passwordVisible.add(false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isExpanded = !isExpanded;
        });
      },
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(left: 20, top: 10, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 2,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        widget.credential.title,
                        style: GoogleFonts.robotoSlab(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Helper().getTitleColor(),
                        ),
                      ),
                    ),
                    Spacer(),
                    IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        //navigate to create screen with edit mode
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (context) => CredCreateScreen(
                                widget.credential, [], Mode.Edit),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
              Visibility(
                visible: isExpanded,
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                "USERNAME",
                                style: GoogleFonts.oswald(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.green,
                                ),
                              ),
                            ),
                            Text(
                              widget.credential.username,
                              style: GoogleFonts.roboto(
                                fontSize: 15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Text(
                          "PASSWORDS",
                          style: GoogleFonts.oswald(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.red,
                          ),
                        ),
                      ),
                      Container(
                        height: 50.00 * widget.credential.passwords.length,
                        child: ListView.builder(
                          itemCount: widget.credential.passwords.length,
                          itemBuilder: (context, index) {
                            return Container(
                              height: 40,
                              child: Row(
                                children: <Widget>[
                                  passwordVisible[index]
                                      ? Text(
                                          EncryptionManager().decrypt(widget
                                              .credential.passwords[index]),
                                          style: GoogleFonts.roboto(
                                            fontSize: 15,
                                          ),
                                        )
                                      : Text(
                                          "**********",
                                          style: GoogleFonts.roboto(
                                            fontSize: 15,
                                          ),
                                        ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.remove_red_eye),
                                    onPressed: () {
                                      setState(() {
                                        passwordVisible[index] =
                                            !passwordVisible[index];
                                      });
                                    },
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
