class Cred {
  String title;
  String username;
  List<String> passwords;

  Cred({this.title, this.username, this.passwords});

  Cred.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        username = json['username'],
        passwords =
            (json['passwords'] as List).map((e) => e.toString()).toList();

  Map<String, dynamic> toJson() => {
        'title': title,
        'username': username,
        'passwords': passwords,
      };
}
