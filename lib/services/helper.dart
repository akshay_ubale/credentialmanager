import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Helper {
  Color getTitleColor() {
    List<Color> colors = [
      Colors.red,
      Colors.blue,
      Colors.yellow,
      Colors.green,
      Colors.black,
      Colors.indigo,
      Colors.orange
    ];

    return colors[Random().nextInt(colors.length)];
  }
}
