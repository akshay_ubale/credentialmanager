import 'dart:convert';

import 'package:encrypt/encrypt.dart';

class EncryptionManager {
  final key = Key.fromUtf8("J@NcRfUjXn2r5u8x/A%D*G-KaPdSgVkY");
  final iv = IV.fromLength(16);
  Encrypter encryptor;

  EncryptionManager() {
    encryptor = Encrypter(AES(key));
  }

  String encrypt(String plainText) {
    return encryptor.encrypt(plainText, iv: iv).base64;
  }

  String decrypt(String encryptedBase64) {
    return encryptor.decrypt(Encrypted(base64.decode(encryptedBase64)), iv: iv);
  }
}
