import 'dart:convert';

import 'package:credmanage/cred_create_screen.dart';
import 'package:credmanage/models/mode.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'models/cred.dart';
import 'cred_widget.dart';

class CredManagerScreen extends StatefulWidget {
  @override
  CredManagerScreenState createState() => CredManagerScreenState();
}

class CredManagerScreenState extends State<CredManagerScreen> {
  List<Cred> creds = [];
  List<String> titles = [];

  //for searchbar in appbar
  Widget appbarTitle;
  Icon actionIcon;

  String searchFilter;

  Future<void> fetchData() async {
    final allTitles = await FirebaseDatabase.instance
        .reference()
        .child(FirebaseAuth.instance.currentUser.uid)
        .once();

    Map<dynamic, dynamic> data = allTitles.value;
    List<Cred> credl = [];
    List<String> titlesl = [];

    if (data == null || data.length <= 0) {
      setState(() {
        creds = credl;
        titles = titlesl;
      });

      return;
    }

    data.keys.forEach((element) {
      titlesl.add(element.toString().toLowerCase());

      credl.add(Cred.fromJson(jsonDecode(data[element.toString()])));
    });

    setState(() {
      creds = credl;
      titles = titlesl;
    });
  }

  @override
  void initState() {
    FirebaseDatabase.instance
        .reference()
        .child(FirebaseAuth.instance.currentUser.uid)
        .onValue
        .listen((event) async {
      await fetchData();
    });
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await fetchData();
    });

    appbarTitle = Text("CREDENTIALS");
    actionIcon = Icon(Icons.search);
    searchFilter = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: appbarTitle,
        actions: [
          IconButton(
            icon: actionIcon,
            onPressed: () {
              if (this.actionIcon.icon == Icons.search) {
                setState(() {
                  actionIcon = Icon(Icons.close);

                  appbarTitle = TextField(
                    style: GoogleFonts.roboto(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.white,
                      ),
                      hintText: "Search....",
                      hintStyle: GoogleFonts.roboto(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                    cursorColor: Colors.white,
                    onChanged: (value) {
                      setState(() {
                        searchFilter = value;
                      });
                    },
                  );
                });
              } else {
                setState(() {
                  appbarTitle = Text("CREDENTIALS");
                  actionIcon = Icon(Icons.search);
                  searchFilter = "";
                });
              }
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: creds.length,
                itemBuilder: (context, index) {
                  if (searchFilter != null &&
                      searchFilter != "" &&
                      !creds[index]
                          .title
                          .toLowerCase()
                          .contains(searchFilter.toLowerCase())) {
                    return Container();
                  }
                  return CredWidget(creds[index]);
                },
              ),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) =>
                  CredCreateScreen(Cred(), titles, Mode.Create),
            ),
          );
        },
        tooltip: 'Add Credentials',
        child: Icon(Icons.add),
      ),
    );
  }
}
