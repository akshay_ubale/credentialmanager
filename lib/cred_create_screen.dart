import 'dart:convert';

import 'package:credmanage/models/mode.dart';
import 'package:credmanage/services/encryption.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'models/cred.dart';

class CredCreateScreen extends StatefulWidget {
  final Cred credential;
  final List<String> availableTitles;
  final Mode mode;

  CredCreateScreen(this.credential, this.availableTitles, this.mode);

  @override
  CredCreateState createState() => CredCreateState();
}

class CredCreateState extends State<CredCreateScreen> {
  final databaseRef = FirebaseDatabase.instance
      .reference()
      .child(FirebaseAuth.instance.currentUser.uid);

  final _formKey = GlobalKey<FormState>();
  Cred credential;
  bool showPassword = false;

  @override
  void initState() {
    credential = Cred(
      title: widget.credential.title,
      username: widget.credential.username,
      passwords: [
        widget.mode == Mode.Edit
            ? EncryptionManager().decrypt(widget.credential.passwords[0])
            : ""
      ],
    );
    showPassword = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CREATE"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              showPassword
                  ? Icons.remove_red_eye
                  : Icons.remove_red_eye_outlined,
            ),
            onPressed: () {
              setState(() {
                showPassword = !showPassword;
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () async {
              if (_formKey.currentState.validate()) {
                //send to firebase
                //encrypt data
                credential.passwords[0] =
                    EncryptionManager().encrypt(credential.passwords[0]);

                await databaseRef
                    .child(credential.title)
                    .set(jsonEncode(credential));
                Navigator.of(context).pop();
              }
            },
          ),
          widget.mode == Mode.Edit
              ? IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () async {
                    //delete
                    await databaseRef.child(credential.title).remove();
                    Navigator.of(context).pop();
                  },
                )
              : Container(),
        ],
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  //Title
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.black,
                        ),
                      ),
                    ),
                    margin: EdgeInsets.all(20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Title",
                        hintStyle: GoogleFonts.robotoSlab(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      initialValue:
                          widget.mode == Mode.Edit ? credential.title : "",
                      readOnly: widget.mode == Mode.Edit,
                      style: GoogleFonts.robotoSlab(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter unique title.";
                        }

                        if (widget.availableTitles
                            .contains(value.trim().toLowerCase())) {
                          return "This title is already used.";
                        }
                        setState(() {
                          credential.title = value;
                        });
                        return null;
                      },
                    ),
                  ),
                  //Username
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Username",
                        hintStyle: GoogleFonts.oswald(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      style: GoogleFonts.oswald(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      initialValue:
                          widget.mode == Mode.Edit ? credential.username : "",
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter username.";
                        }
                        setState(() {
                          credential.username = value;
                        });
                        return null;
                      },
                    ),
                  ),
                  //Password
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: "Password",
                        hintStyle: GoogleFonts.oswald(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      style: GoogleFonts.oswald(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                      obscureText: !showPassword,
                      initialValue: widget.mode == Mode.Edit
                          ? credential.passwords[0]
                          : "",
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter password.";
                        }
                        setState(() {
                          credential.passwords = [value];
                        });
                        return null;
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
