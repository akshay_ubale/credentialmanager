import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:local_auth/local_auth.dart';
import 'cred_manager_screen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<bool> doLocalAuthentication() async {
    bool authenticated = false;
    try {
      authenticated = await _localAuthentication.authenticate(
          localizedReason: '', useErrorDialogs: false, stickyAuth: true);
    } catch (e) {
      print(e);
    }
    return authenticated;
    //SystemNavigator.pop();
  }

  Future<bool> init() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleUSer = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUSer.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    await FirebaseAuth.instance.signInWithCredential(credential);

    return doLocalAuthentication();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final LocalAuthentication _localAuthentication = LocalAuthentication();

  Future<bool> doLocalAuthentication() async {
    bool authenticated = false;
    try {
      authenticated = await _localAuthentication.authenticate(
          localizedReason: '', useErrorDialogs: false, stickyAuth: true);
    } catch (e) {
      print(e);
    }
    return authenticated;
    //SystemNavigator.pop();
  }

  Future<bool> init() async {
    await Firebase.initializeApp();

    final GoogleSignInAccount googleUSer = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUSer.authentication;

    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    await FirebaseAuth.instance.signInWithCredential(credential);

    return doLocalAuthentication();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: init(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          SystemNavigator.pop();
        }

        if (snapshot.data != null && snapshot.data) {
          Future.delayed(
            Duration.zero,
            () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => CredManagerScreen(),
              ));
            },
          );
        }

        return Scaffold(
          body: Center(
            child: Column(
              children: <Widget>[
                Spacer(),
                Image(
                  image: AssetImage('assets/lock.png'),
                  height: 200,
                  width: 200,
                ),
                Text(
                  "CREDENTIAL MANAGER",
                  style: TextStyle(
                      color: Colors.amber,
                      fontSize: 30,
                      fontWeight: FontWeight.w800),
                ),
                Container(
                  height: 30,
                ),
                Spacer(),
              ],
            ),
          ),
        );
      },
    );
  }
}
